<?php

namespace App\View\Components\Website;

use Illuminate\View\Component;

class Nav extends Component
{
    public $classes;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($classes = null)
    {
        $this->classes = $classes;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.website.nav');
    }
}
