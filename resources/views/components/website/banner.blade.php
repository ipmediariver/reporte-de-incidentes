<section class="py-10 md:py-24 bg-dark" id="home">
    <div class="w-11/12 md:w-9/12 mx-auto flex flex-col space-y-7 md:space-y-0 md:flex-row items-center space-x-0 md:space-x-20">
        <div class="w-full md:w-1/2">
            <h1 class="text-white text-3xl md:text-5xl font-bold mb-7 md:mb-10">Reporte De Incidentes</h1>
            <p class="text-white text-lg mb-10">
                RDI te mantiene informado sobre el mantenimiento y la seguridad de todas tus instalaciones a través de mensajes diarios reportados y analizados por agentes especializados en el área.
            </p>
            <a href="mailto:info@rdimx.com" class="bg-secondary text-white rounded py-3 px-4 inline-block shadow hover:underline">
                Solicitar demostración
            </a>
        </div>
        <div class="w-full md:w-1/2">
            <div class="bg-white rounded shadow-lg p-3">
                <img src="/img/RDI-Screenshot-4.png" class="w-full h-auto" alt="">
            </div>
        </div>
    </div>
</section>