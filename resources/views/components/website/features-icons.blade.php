<section class="w-11/12 md:w-9/12 mx-auto py-10 md:py-24">
    <div class="flex flex-col md:flex-row items-end space-x-0 md:space-x-10 space-y-10 md:space-y-0 text-center md:text-left">
        <div class="w-full md:w-1/3">
            <i class="fa fa-comment fa-3x text-secondary mb-7"></i>
            <h3 class="text-2xl font-bold mb-6 text-primary">Notificaciones urgentes</h3>
            <p class="text-base">Recíbe notificaciones via SMS de incidentes que requieren atención urgente.</p>
        </div>
        <div class="w-full md:w-1/3">
            <i class="fa fa-tasks fa-3x text-secondary mb-7"></i>
            <h3 class="text-2xl font-bold mb-6 text-primary">Asignacion de tareas</h3>
            <p class="text-base">Asigna tareas especificas a los agentes para control de productividad.</p>
        </div>
        <div class="w-full md:w-1/3">
            <i class="fa fa-chart-line fa-3x text-secondary mb-7"></i>
            <h3 class="text-2xl font-bold mb-6 text-primary">Descarga de reportes</h3>
            <p class="text-base">Obtén reportes detallados de incidentes automaticamente en tu correo electrónico.</p>
        </div>
    </div>
</section>