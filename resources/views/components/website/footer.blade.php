<footer class="bg-dark py-10 md:py-24" id="contact">
    <div class="w-11/12 md:w-9/12 mx-auto flex flex-col md:flex-row space-y-7 md:space-y-0 items-start">
        <div class="w-full md:w-1/3">
            <div>
                <x-website.logo />
            </div>
            <p class="mt-7 text-white">©{{ date('Y') }} - {{ env('APP_NAME') }}</p>
        </div>
        <div class="w-full md:w-2/3">
            <h3 class="text-white text-2xl mb-6">Contáctanos</h3>
            <p class="text-white">Si deseas obtener más información sobre RDI o como obtenerlo, comunicate con nosotros y nosotros resolveremos todas tus dudas.</p>
            <h4 class="text-lg font-bold text-white mt-7">Correo electrónico:</h4>
            <a href="mailto:info@rdimx.com" class="text-secondary underline">info@rdimx.com<p>
        </div>
    </div>
</footer>