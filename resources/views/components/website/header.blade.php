<header class="py-5 bg-primary text-white shadow relative md:fixed top-0 right-0 left-0 z-30 border-t-4 border-secondary">
    <div class="w-full md:w-9/12 flex flex-col md:flex-row space-y-7 md:space-y-0 items-center mx-auto">
        <x-website.logo />
        <x-website.nav />
    </div>
</header>