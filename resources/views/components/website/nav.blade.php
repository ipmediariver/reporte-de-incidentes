<nav class="flex items-center text-sm md:text-base space-x-4 md:space-x-6 ml-0 md:ml-auto">
    <a href="#home" class="hover:underline hover:text-secondary">Inicio</a>
    <a href="#specs" class="hover:underline hover:text-secondary">Especificaciones</a>
    <a href="mailto:info@rdimx.com" class="hover:underline hover:text-secondary">Demo</a>
    <a href="#contact" class="hover:underline hover:text-secondary">Contacto</a>
</nav>