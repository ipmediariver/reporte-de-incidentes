<section class="py-10 md:py-24 bg-gray-200">
    <div class="w-11/12 md:w-9/12 mx-auto flex flex-col md:flex-row items-center space-y-6 md:space-y-0">
        <div class="w-full md:w-8/12">
            <h3 class="text-3xl md:text-5xl font-bold mb-6 text-primary">Perfecto para usarlo desde cualquier dispositivo</h3>
            <p class="text-base md:text-lg text-primary">RDI está enfocado en brindar la mayor libertad de uso posible, por lo que el acceso desde cualquier dispositivo movil es uno de los beneficios principales de la plataforma.</p>
        </div>
        <div class="w-full md:w-3/12 ml-auto">
            <div class="bg-white shadow-lg rounded p-3">
                <img src="/img/RDI-Screenshot-3.png" class="w-full h-auto" alt="">
            </div>
        </div>
    </div>
</section>