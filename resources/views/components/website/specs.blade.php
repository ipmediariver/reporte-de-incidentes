<section class="py-10 md:py-24 bg-dark" id="specs">
    <div class="w-11/12 md:w-9/12 mx-auto flex flex-col md:flex-row items-center space-x-0 md:space-x-20 space-y-10 md:space-y-0">
        <div class="w-full md:w-3/12">
            <div class="bg-white shadow-lg rounded p-3">
                <img src="/img/RDI-Screenshot-2.png" class="w-full h-auto" alt="">
            </div>
        </div>
        <div class="w-full md:w-9/12">
            <h1 class="font-bold text-3xl md:text-5xl mb-7 text-white">Acerca de RDI</h1>
            <h4 class="text-xl md:text-2xl font-bold text-secondary">El mejor software para reportes de incidentes.</h4>
            <ul class="mt-10 font-bold flex flex-col space-y-5 text-white text-opacity-75">            
                <li>1) Los incidentes registrados pueden incluir hasta 5 fotografías.</li>
                <li>2) Los agentes asignados a cada zona tienen sus tareas registradas.</li>
                <li>3) Check In y Check Out para validar la productividad del personal dedicado a reportar incidentes.</li>
                <li>4) Interfaz amigable e intuitiva desde cualquier dispositivo.</li>
            </ul>
        </div>
    </div>
</section>