<x-guest-layout>
	<x-website.banner />
	<x-website.features-icons />
	<x-website.specs />
	<x-website.responsive />
</x-guest-layout>